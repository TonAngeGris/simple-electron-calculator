const { app, BrowserWindow } = require('electron');
const path = require('path');
const url = require('url');
const { ipcMain } = require('electron');
let mainWindow;

ipcMain.on('close-me', (evt, arg) => {
  app.quit();
});

app.on('window-all-closed', () => {
  if (process.platform != 'darwin') {
    app.quit();
  }
});

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    width: 400,
    height: 655,
    icon: __dirname + '/img/icon.png',
    webPreferences: {
      nodeIntegration: true,
      worldSafeExecuteJavaScript: true,
      allowRunningInsecureContent: true,
    },
    resizable: false,
    autoHideMenuBar: true,
    frame: false,
  });

  mainWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file:',
      slashes: true,
    })
  );

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  ipcMain.on('minimize-me', (evt, arg) => {
    mainWindow.minimize();
  });
});

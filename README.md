# Simple Electron Calculator
![](https://i.imgur.com/pghDury.png)

Just a simple calculator written with [electron.js](https://www.electronjs.org/).

### Requirements
- [Node.js](https://nodejs.org/) 13+
- npm 6+

### Installation
1. Clone the project
```sh
git clone https://gitlab.com/TonAngeGris/simple-electron-calculator.git
cd simple-electron-calculator
```
2. Install dependencies
```sh
npm i
```
3. Then you can:
    - Run the application
    ```sh 
    npm start
    ```
    - Build the application
    ```sh
    npm run dist
    ```
    Then you can find packed and unpacked executors inside `build/` folder.

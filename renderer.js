// renderer listener
const { ipcRenderer } = require('electron');

// closeApp button from html
const closeApp = document.getElementById('closeApp');
// minimizeApp button from html
const minimizeApp = document.getElementById('minimizeApp');
// input field from html
const input = document.querySelector('.input');

const simpleOperations = ['+', '-', '*', '/'];
const complexOperations = ['sqrt', '^-1', '^', 'sin', 'cos', 'tan', 'ctg'];
const brackets = ['(', ')'];

let leftBracketCnt = 0;
let rightBracketCnt = 0;

// if equal was the last operation
let equalFlag = false;
// if there's correct dot in input
let dotFlag = false;
// if 1st num is null
let firstZero = false;

// listener on close button
closeApp.addEventListener('click', () => {
  ipcRenderer.send('close-me');
});

// listener on minimize button
minimizeApp.addEventListener('click', () => {
  ipcRenderer.send('minimize-me');
});

// ? TODO: add expanding button on the left of input to check the whole last one
// OK ? TODO: add PI button (change with ctg button)
// OK TODO: make right back button behavior
// TODO: make right power behavior with brackets and negative numbers

// insert numbers
function insertNum(num) {
  let exp = input.textContent;

  if ((equalFlag && !simpleOperations.includes(exp[exp.length - 1])) || exp === 'NaN') {
    input.textContent = num;
  }

  if (exp[exp.length - 1] === ')') {
    return;
  }

  if (num === '.') {
    // if there's no dot in the number yet
    if (!dotFlag && !equalFlag) {
      // if there's a digit on the left of the number space
      if (exp[exp.length - 1].match(/^[0-9]/)) {
        input.textContent += num;
        dotFlag = true;
      }
    }
    return;
  }
  // #debug
  // console.log(`${input.textContent[input.textContent.length - 1] !== '0'} || ${dotFlag}`);
  if (
    (num === '0' &&
      (!firstZero || dotFlag) &&
      (input.textContent[input.textContent.length - 1] !== '0' || dotFlag)) ||
    (num === '0' && !input.textContent)
  ) {
    input.textContent += num;
    firstZero = true;
    return;
  } else if (num === '0') {
    return;
  }

  if ((equalFlag && !simpleOperations.includes(exp[exp.length - 1])) || exp === 'NaN') {
    input.textContent = num;
  } else {
    input.textContent += num;
  }

  equalFlag = false;
}

// "10+*" is bad...
// insert operations
function insertOper(operArg) {
  let exp = input.textContent;
  let isPrevOper = false;

  if (complexOperations.includes(operArg)) {
    switch (operArg) {
      case 'sqrt': {
        equal();
        input.textContent = Math.sqrt(input.textContent);
        break;
      }
      case '^-1': {
        equal();
        input.textContent = Math.pow(eval(input.textContent), -1);
        break;
      }
      case '^': {
        input.textContent += '^';
        break;
      }
      case 'sin': {
        equal();
        input.textContent = Math.sin(input.textContent);
        break;
      }
      case 'cos': {
        equal();
        input.textContent = Math.cos(input.textContent);
        break;
      }
      case 'tan': {
        equal();
        input.textContent = Math.tan(input.textContent);
        break;
      }
    }
  } else if (brackets.includes(operArg)) {
    if (operArg === '(') {
      if (input.textContent[input.textContent.length - 1] != undefined) {
        if (input.textContent[input.textContent.length - 1].match(/[0-9]/)) {
          return;
        }
      }
      leftBracketCnt++;
    }

    if (operArg === ')') {
      if (leftBracketCnt !== rightBracketCnt + 1) {
        return;
      }
      rightBracketCnt++;
    }

    input.textContent += operArg;
  } else {
    simpleOperations.forEach((operation) => {
      if (operation === exp[exp.length - 1]) {
        isPrevOper = true;
        return;
      }
    });

    if (!input.textContent && ['+', '-'].includes(operArg)) {
      input.textContent += operArg;
    } else if (!input.textContent && ['*', '/'].includes(operArg)) {
      input.textContent += '';
    } else if (!isPrevOper) {
      input.textContent += operArg;
    } else {
      input.textContent += '';
    }
  }

  dotFlag = false;
  firstZero = false;
}

// clean input
function clean() {
  input.textContent = '';
  equalFlag = false;
  dotFlag = false;
  firstZero = false;

  rightBracketCnt = 0;
  leftBracketCnt = 0;
}

// clean 1 symbol 
function back() {
  switch (input.textContent[input.textContent.length - 1]) {
    case '(': {
      leftBracketCnt--;
      break;
    }
    case ')': {
      rightBracketCnt--;
      break;
    }
    case '.': {
      dotFlag = false;
      break;
    }
  }

  if (
    input.textContent[input.textContent.length - 1] === '0' &&
    input.textContent.length === 1 &&
    firstZero
  ) {
    firstZero = false;
  }

  input.textContent = input.textContent.substring(0, input.textContent.length - 1);
}

// Calculate input string
function equal() {
  // if (input.textContent.includes('0/')) {
  //   input.textContent = 'NaN';
  //   equalFlag = true;
  //   return;
  // }

  if (simpleOperations.includes(input.textContent[input.textContent.length - 1])) {
    input.textContent = input.textContent.slice(0, -1);
  }

  if (input.textContent.includes('^') && input.textContent[input.textContent.length - 1] !== '^') {
    let caretSplit = input.textContent.split('^');
    let firstOperIdx = caretSplit[1].split('').findIndex((oper) => oper.match(/[\+\-\*\/]/));
    if (firstOperIdx === -1) {
      firstOperIdx = 1;
    }

    let power = caretSplit[1].slice(0, firstOperIdx);
    caretSplit[0] = Math.pow(caretSplit[0], power);
    caretSplit[1] = caretSplit[1].slice(firstOperIdx);

    input.textContent = caretSplit.join('');
  }

  if (input.textContent.includes('π')) {
    let exp = input.textContent;
    console.log(exp.length);

    for (let i = 0; i < exp.length; i++) {
      console.log(i);
      if (exp[i] === 'π') {
        if (exp.length > 1 && exp[i - 1].match(/[0-9]/)) {
          exp = exp.substring(0, i) + '*' + exp.substring(i);
          i++;
        }

        exp = exp.split('');
        exp[i] = Math.PI;
        exp = exp.join('');
      }
    }
    input.textContent = exp;
    // input.textContent = input.textContent
    //   .split('')
    //   .map((sym) => (sym === 'π' ? Math.PI : sym))
    //   .join('');
    console.log(input.textContent);
  }

  if (input.textContent) {
    try {
      input.textContent = eval(input.textContent);
    } catch (err) {
      if (err instanceof SyntaxError) {
        while (input.textContent[0] === '0') {
          input.textContent = input.textContent.slice(1);
        }

        input.textContent = eval(input.textContent);
      }
    }
  }

  equalFlag = true;
  dotFlag = false;
  firstZero = false;
  rightBracketCnt = 0;
  leftBracketCnt = 0;
}
